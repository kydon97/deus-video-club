import React from "react";
import { getAllMovies, getCountOfMovies, rentMovie } from "../utils/api";
import "../styles/dashboard.css";
import Menu from "./Menu";
import MoviesCards from "./MoviesCards";
import Rentals from "./Rentals";
import Profile from "./Profile";
import Header from "./Header";
import { BrowserRouter as Router, Route, Link, Redirect, withRouter  } from "react-router-dom";

class MoviesScreen extends React.Component {
  state = {
    count: 0,
    cards: [],
    requestMade: false,
    isClickedMovies:false,
    isClickedRentals:false,
    isClickedProfile:false,
    isClickedLogOut:false
  };

  shouldComponentUpdate() {

    if (this.state.cards.length !== 0) {
      if(this.state.isClickedRentals === true || this.state.isClickedProfile === true || this.state.isClickedLogOut === true) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  componentDidMount() {
    this.setState({
      requestMade: true,
    });
  }

  render() {

    if(this.state.isClickedRentals === true) {
      return (
        <Router>
      <Redirect to="/rentals"/>;
      <Route exact path="/rentals" component={withRouter(() =>  <Rentals username={this.props.username} token={this.props.token}/>)}/>      
     </Router>
      )
    } else if (this.state.isClickedProfile) {
      return (
        <Router>
      <Redirect to="/profile"/>;
      <Route exact path="/profile" component={withRouter(() =>  <Profile username={this.props.username} token={this.props.token}/>)}/>      
     </Router>
      )
    } else if (this.state.isClickedLogOut) {
      return (
        <Router>
      <Redirect to="/"/>;
      <Route exact path="/" component={withRouter(() =>  <Header/>)}/>      
     </Router>
      )
    }else if (this.state.isClickedMovies) {
      return (
        <Router>
      <Redirect to="/dashboard"/>;
      <Route exact path="/dashboard" component={withRouter(() =>  <MoviesScreen username={this.props.username} token={this.props.token}/>)}/>      
     </Router>
      )
    }else {
      const changePages = (type) => {
        if (type === "rentals") {
          this.setState({
            isClickedMovies:false,
            isClickedRentals:true,
            isClickedProfile:false,
            isClickedLogOut:false
         });
        } else if (type === "profile") {
          this.setState({
            isClickedMovies:false,
            isClickedRentals:false,
            isClickedProfile:true,
            isClickedLogOut:false
         });
        } else if (type === "logout") {
          this.setState({
            isClickedMovies:false,
            isClickedRentals:false,
            isClickedProfile:false,
            isClickedLogOut:true
         });
        } else if (type === "movies") {
            this.setState({
                isClickedMovies:true,
                isClickedRentals:false,
                isClickedProfile:false,
                isClickedLogOut:false
             });
        }
    }

    function rentMovieRequest(uuid,token) {
        const textOfDescription = document.getElementById("textOfDescription");
        const rentButton = document.getElementById("rentMovieButton");
        rentButton.style.display = "none";
        textOfDescription.textContent = "Please wait...";
        textOfDescription.style.paddingTop = "50px";


        var formData = new FormData();
        formData.append("movie", uuid);
        rentMovie(formData,token).then(res => {
                textOfDescription.style.whiteSpace = "pre";
                textOfDescription.textContent =`You have successfully rent this movie! \r\n`;
                textOfDescription.textContent += ` \r\n`;
                textOfDescription.textContent += ` Details: \r\n`;
                textOfDescription.textContent += ` \r\n`;
                textOfDescription.textContent += `  Title: ${res.movie} \r\n`;
                textOfDescription.textContent += ` Rental Date: ${res.rental_date} \r\n`; 
                if (res === 400) {
                    textOfDescription.textContent = `You have an active rent for this movie`;
                }
                rentButton.style.display = "block";
          }).catch(error => {
              textOfDescription.textContent = "Something went wrong... Try again in a while."
            console.log("error: " + error);
          })
        
    }
    function closeDescriptionSection() {
        const descriptionSection = document.getElementById("description");
        descriptionSection.style.display="none";
    }
    if (this.state.cards.length !== 0) {
      return (
        <div>
          <p style={{ color: "white", padding:"2em 0", fontSize:"24px" }}>Welcome {this.props.username}</p>
      <Menu changePages = {changePages}/>
          <p style={{color: "white",padding:"4em 0 2em 0", textTransform: "uppercase", textShadow: "2px 2px 4px red", fontSize:"19px"}}> check out our awesome movies</p>
          <MoviesCards cards={this.state.cards}/>
          <div id="description" className="descriptionSection modal-content-dashboard animate">
            <button onClick={closeDescriptionSection} className="closeSection">
              &times;
            </button>
            <p id="textOfDescription"></p>
            <p id="uuidOfMovie" style={{display:"none"}}></p>
            <button id="rentMovieButton" className="seeDescription" onClick={() => rentMovieRequest(document.getElementById("uuidOfMovie").textContent, this.props.token)}>
                    Rent Now
             </button>
          </div>
        </div>
      );
    } else if (this.state.requestMade === false) {
      getCountOfMovies(this.props.token)
        .then((res) => {
          this.setState({
            count: res.count
          });
          getAllMovies(this.props.token, this.state.count)
            .then((res) => {
              this.setState({
                cards: res.results,
              });
            })
            .catch((error) => {
              console.log(error);
            });
        })
        .catch((error) => {
          console.log(error);
        });
      return <p style={{ color: "white" }}>Welcome</p>;
    } else {
      return <p style={{ color: "white" }}>Please Wait...</p>;
    }
  }
}
  }

export default MoviesScreen;
