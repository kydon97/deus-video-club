import React from "react";
import "../styles/dashboard.css";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  withRouter,
} from "react-router-dom";
import MoviesScreen from "./MoviesScreen";
import Header from "./Header";
import Profile from "./Profile";
import Menu from "./Menu";
import { getCountOfRentals, returnMovie , getRentals } from "../utils/api";
import ActiveRentalsCards from "./ActiveRentalsCards";

class Rentals extends React.Component {
  state = {
    count:null,
    isClickedMovies: false,
    isClickedRentals: false,
    isClickedProfile: false,
    isClickedLogOut: false,
    firstName: "",
    lastName:"",
    email:"",
    wallet: "",
    rentals: [],
    titledesc:false,
    rentaldesc:false,
    retunrdesc:false,
    page:1
  };

  componentDidMount() {

  getCountOfRentals(this.props.token).then(res =>{
   this.setState({
     count: res.count
   })
  getRentals(this.props.token,this.state.count).then(res =>{
    this.setState({
      rentals : res.results
    })
    console.log(this.state.rentals);
}).catch(error => {
    console.log("Error: " + error);
})
}).catch(error => {
    console.log("Error: " + error);
})

  }
  render() {
    if(this.state.count === null) {
      return (
         <p style={{ color: "white" }}>Please Wait...</p>
      )
  }else {
    if (this.state.isClickedRentals === true) {
      return (
        <Router>
          <Redirect to="/rentals" />;
          <Route
            exact
            path="/rentals"
            component={withRouter(() => (
              <Rentals
                username={this.props.username}
                token={this.props.token}
              />
            ))}
          />
        </Router>
      );
    } else if (this.state.isClickedProfile) {
      return (
        <Router>
          <Redirect to="/profile" />;
          <Route
            exact
            path="/profile"
            component={withRouter(() => (
              <Profile
                username={this.props.username}
                token={this.props.token}
              />
            ))}
          />
        </Router>
      );
    } else if (this.state.isClickedLogOut) {
      return (
        <Router>
          <Redirect to="/" />;
          <Route
            exact
            path="/"
            component={withRouter(() => (
              <Header />
            ))}
          />
        </Router>
      );
    } else if (this.state.isClickedMovies) {
      return (
        <Router>
          <Redirect to="/dashboard" />;
          <Route
            exact
            path="/dashboard"
            component={withRouter(() => (
              <MoviesScreen
                username={this.props.username}
                token={this.props.token}
              />
            ))}
          />
        </Router>
      );
    } else if(this.state.update === true){
      return (
        <Router>
          <Redirect to="/rentals" />;
          <Route
            exact
            path="/rentals"
            component={withRouter(() => (
              <Rentals
                username={this.props.username}
                token={this.props.token}
              />
            ))}
          />
        </Router>
      );
    }else {
      const returnMovieRequest = (uuid) => {
       const waitMessage = document.getElementById("wait");
       const successMessage = document.getElementById("success");
       const errorMessage = document.getElementById("error");

       waitMessage.style.display = "block";

        returnMovie(this.props.token,uuid).then(res =>{
         if(res ===200) {
           this.setState({
             update: true
           })
            waitMessage.style.display = "none";
            successMessage.style.display = "block";
         } else if(res === 400) {
          errorMessage.textContent = "You have already return this movie or you don't have enough money in the wallet.";
          waitMessage.style.display = "none";
          successMessage.style.display = "none";
          errorMessage.style.display = "block";
         }else {
          waitMessage.style.display = "none";
          successMessage.style.display = "none";
          errorMessage.style.display = "block";
         }


      }).catch(error => {
          console.log("Error: " + error);
      })
      }
      // const pagination = () => {
      //   var table, rows, numberOfMovies, moviesPerPage , numberOfPages;
      //   table = document.getElementById("activerentals");
      //   rows = table.rows;
      //   numberOfMovies = rows.length - 1;
      //   moviesPerPage = 5;
      //   numberOfPages = Math.ceil(numberOfMovies/moviesPerPage);
      //   // for(let i=0; i< moviesPerPage; i++) {
      //   //   rows[i].style.opacity = "1";
      //   // }
      //   for(let i=0; i< moviesPerPage; i++) {
      //     rows[i].style.opacity = "0";
      //   }
      //   if(this.state.page + 1 <= numberOfPages) {
      //     this.setState({
      //       page: this.state.page+1
      //     })
      //   } else {
      //     this.setState({
      //       page: 1
      //     })
      //   }
      //   if(this.state.page ===1 ) {
      //   for(let i=0; i< moviesPerPage; i++) {
      //     rows[i].style.opacity = "1";
      //     // rows[i].insertBefore(rows[i], rows[this.state.page * this.state.page + 1 ]);
      //   }
      //   } else {
      //     let j=0;
      //     for (let i = this.state.page * this.state.page + 1 ; i < ((this.state.page * this.state.page + 1) + moviesPerPage); i++) {
      //       if(i< rows.length) {
      //         rows[j].parentNode.insertBefore(rows[j], rows[i]);
      //         j++;
      //       }  
      //     }
      //   }
    
      // }
      const  sortPerTitle = () => {
        var table, rows, switching, i, x, y, shouldSwitch;
        table = document.getElementById("activerentals");
        switching = true;
        /*Make a loop that will continue until
        no switching has been done:*/
        while (switching) {
          //start by saying: no switching is done:
          switching = false;
          rows = table.rows;
          /*Loop through all table rows (except the
          first, which contains table headers):*/
          for (i = 1; i < (rows.length - 1); i++) {
            //start by saying there should be no switching:
            shouldSwitch = false;
            /*Get the two elements you want to compare,
            one from current row and one from the next:*/
            x = rows[i].getElementsByTagName("TD")[0];
            y = rows[i + 1].getElementsByTagName("TD")[0];
            if(this.state.titledesc) {
              if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                //if so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
              }
            } else {
              if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                //if so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
              }
            }
            this.setState({
              titledesc : !this.state.titledesc
            })
            //check if the two rows should switch place:
          
          }
          if (shouldSwitch) {
            /*If a switch has been marked, make the switch
            and mark that a switch has been done:*/
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
          }
        }
      }

      const sortPerRentalDate = () => {
        var table, rows, switching, i, x, y, shouldSwitch;
        table = document.getElementById("activerentals");
        switching = true;
        while (switching) {
          switching = false;
          rows = table.rows;
          for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[1];
            y = rows[i + 1].getElementsByTagName("TD")[1];
            if(this.state.rentaldesc) {
              if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                shouldSwitch = true;
                break;
              }
            } else {
              if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                shouldSwitch = true;
                break;
              }
            }
            this.setState({
              rentaldesc : !this.state.rentaldesc
            })
          }
          if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
          }
        }
      }
      const sortPerReturnDate = () => {
        var table, rows, switching, i, x, y, shouldSwitch;
        table = document.getElementById("activerentals");
        switching = true;
        while (switching) {
          switching = false;
          rows = table.rows;
          for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[2];
            y = rows[i + 1].getElementsByTagName("TD")[2];
            if(this.state.returndesc) {
              if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                shouldSwitch = true;
                break;
              }
            } else {
              if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                shouldSwitch = true;
                break;
              }
            }
            this.setState({
              returndesc : !this.state.returndesc
            })
          }
          if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
          }
        }
      }
      const changePages = (type) => {
        if (type === "rentals") {
          this.setState({
            isClickedMovies: false,
            isClickedRentals: true,
            isClickedProfile: false,
            isClickedLogOut: false,
          });
        } else if (type === "profile") {
          this.setState({
            isClickedMovies: false,
            isClickedRentals: false,
            isClickedProfile: true,
            isClickedLogOut: false,
          });
        } else if (type === "logout") {
          this.setState({
            isClickedMovies: false,
            isClickedRentals: false,
            isClickedProfile: false,
            isClickedLogOut: true,
          });
        } else if (type === "movies") {
          this.setState({
            isClickedMovies: true,
            isClickedRentals: false,
            isClickedProfile: false,
            isClickedLogOut: false,
          });
        }
      };
      if(this.props.admin === true ) {

        return (

          <div style={{width:"100%"}}>
          <Menu changePages={changePages} admin={true}/>
            <p style={{ color: "white", padding:"2em 0", fontSize: "24px" }}>
        Welcome {this.props.username}, this is all users Rentals:
              </p>
              {this.state.rentals.length !== 0 ? null : <p style={{ color: "white", padding: "2em 0", fontSize: "18px" }}>Not active rentals.</p>}
            <div style={{width:"100%", margin:"auto", display:"flex"}}>
  
              {/* <button style={{  backgroundColor:"black",border:"none",color:"white",cursor: "pointer"}} onClick={pagination}>Prev Page</button> */}
              {/* <button style={{  backgroundColor:"black",border:"none",color:"white",cursor: "pointer"}} onClick={pagination}>Next Page</button> */}
  
            </div>
            {this.state.rentals.length !== 0 ? <table  id="activerentals">
              <tr style={{backgroundColor: "rgba(30, 139, 195, 1) !important"}}>
                  <th style={{cursor:"pointer"}}onClick={() => sortPerTitle(true)}>Title</th>
                  <th style={{cursor:"pointer"}}onClick={sortPerRentalDate}>User</th>
                </tr>
               <ActiveRentalsCards admin={true} activeRentals={this.state.rentals} returnMovieRequest={returnMovieRequest} allRentals={true}/>
              </table> : null}
          </div>
        
        );

      } else {

        return (

          <div style={{width:"100%"}}>
          <Menu changePages={changePages} />
            <p style={{ color: "white", padding:"2em 0", fontSize: "24px" }}>
        Welcome {this.props.username}, this is your Rentals:
              </p>
              {this.state.rentals.length !== 0 ? null : <p style={{ color: "white", padding: "2em 0", fontSize: "18px" }}>You don't have any rentals in your account.</p>}
              <p id="wait" style={{ color: "white", padding: "2em 0", fontSize: "18px", display:"none" }}>
              Wait until we return and charge your wallet for you...
            </p>
            <p  id="success" style={{ color: "green", padding: "2em 0", fontSize: "18px", display:"none" }}>
              Nice! You return the movie, your wallet is updated.
            </p>
            <p  id="error" style={{ color: "red", padding: "2em 0", fontSize: "18px", display:"none" }}>
              Something Went Wrong...Try Again in a while...
            </p>
            <div style={{width:"100%", margin:"auto", display:"flex"}}>
  
              {/* <button style={{  backgroundColor:"black",border:"none",color:"white",cursor: "pointer"}} onClick={pagination}>Prev Page</button> */}
              {/* <button style={{  backgroundColor:"black",border:"none",color:"white",cursor: "pointer"}} onClick={pagination}>Next Page</button> */}
  
            </div>
            {this.state.rentals.length !== 0 ? <table  id="activerentals">
              <tr style={{backgroundColor: "rgba(30, 139, 195, 1) !important"}}>
                  <th style={{cursor:"pointer"}}onClick={() => sortPerTitle(true)}>Title</th>
                  <th style={{cursor:"pointer"}}onClick={sortPerRentalDate}>Rental Date</th>
                  <th style={{cursor:"pointer"}}onClick={sortPerReturnDate}>Return Date</th>
                  <th>Click to return a movie</th>
                </tr>
               <ActiveRentalsCards activeRentals={this.state.rentals} returnMovieRequest={returnMovieRequest} allRentals={true}/>
              </table> : null}
          </div>
        
        );
      }
    }
  }
}
}

export default Rentals;
