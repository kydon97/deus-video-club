import React from "react";
import "../styles/dashboard.css";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  withRouter,
} from "react-router-dom";
import Rentals from "./Rentals";
import MoviesScreen from "./MoviesScreen";
import Header from "./Header";
import Menu from "./Menu";
import { getProfile , getActiveRentals, returnMovie , getCountOfActiveRentals } from "../utils/api";
import ProfileCard from "./ProfileCard";
import ActiveRentalsCards from "./ActiveRentalsCards";

class Profile extends React.Component {
  state = {
    count:null,
    isClickedMovies: false,
    isClickedRentals: false,
    isClickedProfile: false,
    isClickedLogOut: false,
    firstName: "",
    lastName:"",
    email:"",
    wallet: "",
    activeRentals: [],
    titledesc:false,
    rentaldesc:false,

  };
  
  componentDidMount() {
    getProfile(this.props.token).then(res =>{
        this.setState({
          firstName : res.first_name,
          lastName : res.last_name,
          email: res.email,
          wallet: res.wallet
        })
    }).catch(error => {
        console.log("Error: " + error);
    })
  
  getCountOfActiveRentals(this.props.token).then(res =>{
   this.setState({
     count: res.count
   })
  getActiveRentals(this.props.token,this.state.count).then(res =>{
    this.setState({
      activeRentals : res.results
    })
}).catch(error => {
    console.log("Error: " + error);
})
}).catch(error => {
    console.log("Error: " + error);
})

  }
  render() {
    if(this.state.count === null) {
        return (
           <p style={{ color: "white" }}>Please Wait...</p>
        )
    }else {
    if (this.state.isClickedRentals === true) {
      return (
        <Router>
          <Redirect to="/rentals" />;
          <Route
            exact
            path="/rentals"
            component={withRouter(() => (
              <Rentals
                username={this.props.username}
                token={this.props.token}
              />
            ))}
          />
        </Router>
      );
    } else if (this.state.isClickedProfile) {
      return (
        <Router>
          <Redirect to="/profile" />;
          <Route
            exact
            path="/profile"
            component={withRouter(() => (
              <Profile
                username={this.props.username}
                token={this.props.token}
              />
            ))}
          />
        </Router>
      );
    } else if (this.state.isClickedLogOut) {
      return (
        <Router>
          <Redirect to="/" />;
          <Route
            exact
            path="/"
            component={withRouter(() => (
              <Header />
            ))}
          />
        </Router>
      );
    } else if (this.state.isClickedMovies) {
      return (
        <Router>
          <Redirect to="/dashboard" />;
          <Route
            exact
            path="/dashboard"
            component={withRouter(() => (
              <MoviesScreen
                username={this.props.username}
                token={this.props.token}
              />
            ))}
          />
        </Router>
      );
    } else if(this.state.update === true){
      return (
        <Router>
          <Redirect to="/profile" />;
          <Route
            exact
            path="/profile"
            component={withRouter(() => (
              <Profile
                username={this.props.username}
                token={this.props.token}
              />
            ))}
          />
        </Router>
      );
    } else {

      // const updateRentals = () => {
      //   this.setState({
      //     stop: true
      //   })
      //   document.getElementById("success").style.display = "none";
      //   getCountOfActiveRentals(this.props.token).then(res =>{
      //     this.setState({
      //       count: res.count
      //     })
      //    getActiveRentals(this.props.token,this.state.count).then(res =>{
      //      this.setState({
      //        activeRentals : res.results
      //      })
      //  }).catch(error => {
      //      console.log("Error: " + error);
      //  })
      //  }).catch(error => {
      //      console.log("Error: " + error);
      //  })
       
      // }
    
      const returnMovieRequest = (uuid) => {
       const waitMessage = document.getElementById("wait");
       const successMessage = document.getElementById("success");
       const errorMessage = document.getElementById("error");

       waitMessage.style.display = "block";

        returnMovie(this.props.token,uuid).then(res =>{
         if(res ===200) {
           this.setState({
             update:true
           });
            waitMessage.style.display = "none";
            successMessage.style.display = "block";
         } else if(res === 400) {
           errorMessage.textContent = "You have already return this movie or you don't have enough money in the wallet.";
          waitMessage.style.display = "none";
          successMessage.style.display = "none";
          errorMessage.style.display = "block";
         }else {
          waitMessage.style.display = "none";
          successMessage.style.display = "none";
          errorMessage.style.display = "block";
         }


      }).catch(error => {
          console.log("Error: " + error);
      })
      }
      const  sortPerTitle = () => {
        var table, rows, switching, i, x, y, shouldSwitch;
        table = document.getElementById("activerentals");
        switching = true;
        /*Make a loop that will continue until
        no switching has been done:*/
        while (switching) {
          //start by saying: no switching is done:
          switching = false;
          rows = table.rows;
          /*Loop through all table rows (except the
          first, which contains table headers):*/
          for (i = 1; i < (rows.length - 1); i++) {
            //start by saying there should be no switching:
            shouldSwitch = false;
            /*Get the two elements you want to compare,
            one from current row and one from the next:*/
            x = rows[i].getElementsByTagName("TD")[0];
            y = rows[i + 1].getElementsByTagName("TD")[0];
            if(this.state.titledesc) {
              if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                //if so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
              }
            } else {
              if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                //if so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
              }
            }
            this.setState({
              titledesc : !this.state.titledesc
            })
            //check if the two rows should switch place:
          
          }
          if (shouldSwitch) {
            /*If a switch has been marked, make the switch
            and mark that a switch has been done:*/
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
          }
        }
      }

      const sortPerRentalDate = () => {
        var table, rows, switching, i, x, y, shouldSwitch;
        table = document.getElementById("activerentals");
        switching = true;
        while (switching) {
          switching = false;
          rows = table.rows;
          for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[1];
            y = rows[i + 1].getElementsByTagName("TD")[1];
            if(this.state.rentaldesc) {
              if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                shouldSwitch = true;
                break;
              }
            } else {
              if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                shouldSwitch = true;
                break;
              }
            }
            this.setState({
              rentaldesc : !this.state.rentaldesc
            })
          }
          if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
          }
        }
      }
      const changePages = (type) => {
        if (type === "rentals") {
          this.setState({
            isClickedMovies: false,
            isClickedRentals: true,
            isClickedProfile: false,
            isClickedLogOut: false,
          });
        } else if (type === "profile") {
          this.setState({
            isClickedMovies: false,
            isClickedRentals: false,
            isClickedProfile: true,
            isClickedLogOut: false,
          });
        } else if (type === "logout") {
          this.setState({
            isClickedMovies: false,
            isClickedRentals: false,
            isClickedProfile: false,
            isClickedLogOut: true,
          });
        } else if (type === "movies") {
          this.setState({
            isClickedMovies: true,
            isClickedRentals: false,
            isClickedProfile: false,
            isClickedLogOut: false,
          });
        }
      };

      // if(this.state.update === true ) {

      //   return (

      //     <div style={{width:"100%"}}>
      //       <p style={{ color: "white", padding: "2em 0", fontSize: "24px" }}>
      //         Welcome {this.props.username}, this is your profile. 
      //       </p>
      //       <Menu changePages={changePages} />
      //       <p style={{ color: "white", paddingTop:"1em", fontSize: "24px" }}>
      //         Personal Details:
      //       </p>
      //       <ProfileCard firstName={this.state.firstName} lastName={this.state.lastName} email={this.state.email} wallet={this.state.wallet}/>
            
      //       <p style={{ color: "white", padding:"2em 0", fontSize: "24px" }}>
      //         Your Active Rentals:
      //         </p>
      //         {this.state.activeRentals.length !== 0 ? null : <p style={{ color: "white", padding: "2em 0", fontSize: "18px" }}>You don't have any active rentals in your account.</p>}
  
      //         <p id="wait" style={{ color: "white", padding: "2em 0", fontSize: "18px", display:"none" }}>
      //         Wait until we return and charge your wallet for you...
      //       </p>
      //       <p  id="success" style={{ color: "green", padding: "2em 0", fontSize: "18px", display:"none" }}>
      //         Nice! You return the movie, your wallet is updated.
      //       </p>
      //       <p  id="error" style={{ color: "red", padding: "2em 0", fontSize: "18px", display:"none" }}>
      //         Something Went Wrong...Try Again in a while...
      //       </p>
      //         {this.state.activeRentals.length !== 0 ? <table  id="activerentals">
      //            <tr style={{backgroundColor: "rgba(30, 139, 195, 1) !important"}}>
      //             <th style={{cursor:"pointer"}}onClick={() => sortPerTitle(true)}>Title</th>
      //             <th style={{cursor:"pointer"}}onClick={sortPerRentalDate}>Rental Date</th>
      //             <th>Click to return a movie</th>
      //           </tr>
      //          <ActiveRentalsCards activeRentals={this.state.activeRentals} returnMovieRequest={returnMovieRequest}/>
      //         </table> : null}
      //     </div>
        
      //   );
      // } else {
        // if(this.state.update === true ) {
        //   updateRentals();
        // }

        return (

          <div style={{width:"100%"}}>
            <p style={{ color: "white", padding: "2em 0", fontSize: "24px" }}>
              Welcome {this.props.username}, this is your profile. 
            </p>
            <Menu changePages={changePages} />
            <p style={{ color: "white", paddingTop:"1em", fontSize: "24px" }}>
              Personal Details:
            </p>
            <ProfileCard firstName={this.state.firstName} lastName={this.state.lastName} email={this.state.email} wallet={this.state.wallet}/>
            
            <p style={{ color: "white", padding:"2em 0", fontSize: "24px" }}>
              Your Active Rentals:
              </p>
              {this.state.activeRentals.length !== 0 ? null : <p style={{ color: "white", padding: "2em 0", fontSize: "18px" }}>You don't have any active rentals in your account.</p>}
  
              <p id="wait" style={{ color: "white", padding: "2em 0", fontSize: "18px", display:"none" }}>
              Wait until we return and charge your wallet for you...
            </p>
            <p  id="success" style={{ color: "green", padding: "2em 0", fontSize: "18px", display:"none" }}>
              Nice! You return the movie, your wallet is updated.
            </p>
            <p  id="error" style={{ color: "red", padding: "2em 0", fontSize: "18px", display:"none" }}>
              Something Went Wrong...Try Again in a while...
            </p>
              {this.state.activeRentals.length !== 0 ? <table  id="activerentals">
                 <tr style={{backgroundColor: "rgba(30, 139, 195, 1) !important"}}>
                  <th style={{cursor:"pointer"}}onClick={() => sortPerTitle(true)}>Title</th>
                  <th style={{cursor:"pointer"}}onClick={sortPerRentalDate}>Rental Date</th>
                  <th>Click to return a movie</th>
                </tr>
               <ActiveRentalsCards activeRentals={this.state.activeRentals} returnMovieRequest={returnMovieRequest}/>
              </table> : null}
          </div>
        
        );
      }
     
    }
  }
}
// }

export default Profile;
