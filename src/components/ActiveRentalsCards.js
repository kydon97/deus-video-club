
import React from 'react';
import '../styles/dashboard.css';


class ActiveRentalsCards extends React.Component {
  render() {
    if(this.props.allRentals) {
      if(this.props.activeRentals.length !== 0) {

        if(this.props.admin === true) {
          return (
            <React.Fragment>
        {this.props.activeRentals.map((card) => (
         <tr style={{width:"100%"}}>
           <td>{card.movie}</td>
           <td>{card.user}</td>
         </tr>
        ))}
  </React.Fragment>
      )
        } else {
          return (
            <React.Fragment>
        {this.props.activeRentals.map((card) => (
         <tr style={{width:"100%", opacity:"1"}}>
           <td>{card.movie}</td>
           <td>{card.rental_date}</td>
           {card.return_date ? <td>{card.return_date}</td> : <td>-</td>}
           {card.return_date ? <td>-</td> : <button className="returnRentalButton" onClick={() => this.props.returnMovieRequest(card.uuid)}>Return</button>}
         </tr>
        ))}
    </React.Fragment>
      )
    }
    } else {
        return (
          <p style={{ color: "white", padding: "2em 0", fontSize: "18px", display:"none" }}>No  Rentals for users.</p>
        )
      }
    } else {
      if(this.props.activeRentals.length !== 0) {
        return (
          <React.Fragment>
      {this.props.activeRentals.map((card) => (
       <tr style={{width:"100%"}}>
         <td>{card.movie}</td>
         <td>{card.rental_date}</td>
         <button className="returnRentalButton" onClick={() => this.props.returnMovieRequest(card.uuid)}>Return</button>
       </tr>
      ))}
</React.Fragment>
    )
      } else {
        return (
          <p style={{ color: "white", padding: "2em 0", fontSize: "18px", display:"none" }}>You don't have any active rentals in your account.</p>
        )
      }
  }
}
}


export default ActiveRentalsCards;