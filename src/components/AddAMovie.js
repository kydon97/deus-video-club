import React from "react";
import "../styles/dashboard.css";
import deus_ex_machina from '../images/deus_ex_machina.png';

import {
  BrowserRouter as Router,
  Route,
  Redirect,
  withRouter,
} from "react-router-dom";
import Rentals from "./Rentals";
import Header from "./Header";
import Menu from "./Menu";
import {  getCategories, addNewMovie } from "../utils/api";

class AddAMovie extends React.Component {

    state = {
        isClickedMovies: false,
        isClickedAddAMovie: false,
        isClickedLogOut: false
      };
      
    render() {
        function checkIfInputsAreEmpty() {
            const username = document.getElementById("title").value;
            const categories = document.getElementById("categories").value;
            const addButton = document.getElementById("submit");
            if(username === "" || categories === "") {
                addButton.style.pointerEvents = "none";
                addButton.style.cursor = "not-allowed";
                addButton.style.opacity = ".8";
            } else {
                addButton.style.pointerEvents = "auto";
                addButton.style.cursor = "pointer";
              addButton.style.opacity = "1re";
            }
          }
        function closeLoginForm() { 
            const loginForm = document.getElementById("login");
            const addButton = document.getElementById("addMovie");
            loginForm.style.display="none";
            addButton.style.display="block";
          }

        if (this.state.isClickedMovies === true) {
            return (
              <Router>
                <Redirect to="/rentals" />;
                <Route
                  exact
                  path="/rentals"
                  component={withRouter(() => (
                    <Rentals
                      username={this.props.username}
                      token={this.props.token}
                      admin={true}
                    />
                  ))}
                />
              </Router>
            );
          } else if (this.state.isClickedAddAMovie) {
            return (
              <Router>
                <Redirect to="/addAMovie" />;
                <Route
                  exact
                  path="/addAMovie"
                  component={withRouter(() => (
                    <AddAMovie
                      username={this.props.username}
                      token={this.props.token}
                    />
                  ))}
                />
              </Router>
            );
          } else if (this.state.isClickedLogOut) {
            return (
              <Router>
                <Redirect to="/" />;
                <Route
                  exact
                  path="/"
                  component={withRouter(() => (
                    <Header />
                  ))}
                />
              </Router>
            );
          } else {
        const changePages = (type) => {
            if (type === "addAMovie") {
              this.setState({
                isClickedMovies: false,
                isClickedAddAMovie: true,
                isClickedLogOut: false,
              });
            } else if (type === "logout") {
              this.setState({
                isClickedMovies: false,
                isClickedAddAMovie: false,
                isClickedLogOut: true,
              });
            } else if (type === "movies") {
              this.setState({
                isClickedMovies: true,
                isClickedAddAMovie: false,
                isClickedLogOut: false,
              });
            }
          };
          function showAddModal() {   
            const loginForm = document.getElementById("login");
            const loginButton = document.getElementById("addMovie");
            loginForm.style.display="block";
            loginButton.style.display="none";
            }
            const addMovieRequest = () => {

                getCategories(this.props.token).then(res => {
                    let cat = ["Drama"];
                    const title = document.getElementById("title").value;
                    const pubdate = document.getElementById("pubdate").value;
                    const duration = document.getElementById("duration").value;
                    const rating = document.getElementById("rating").value;
                    const description = document.getElementById("description").value;
                    const categories = document.getElementById("categories").value;
                                        
                    var formData = new FormData();
            formData.append("title", title);
            if(pubdate !== "") {
                formData.append("pub_date", pubdate)
            }
            if(duration !== "") {
                formData.append("duration", duration);
            }
            if(rating !== "") {
                formData.append("rating", rating);  
            }
            if(description !== "") {  
                formData.append("description", description); 
            }
            formData.append("categories", JSON.stringify(res[0].name));   
            addNewMovie(this.props.token , formData).then(res => {
            }).catch(error => {
                console.log("Error : " + error);
            })
                }).catch(error => {
                    console.log("Error: " + error);
                })
          
            }
        return (

            <div style={{width:"100%"}}>
            <p style={{ color: "white", padding: "2em 0", fontSize: "24px" }}>
              Add A Movie
            </p>
            <button id="addMovie" className="addAMovieButton" style={{margin:"auto"}} onClick={showAddModal}>Add a movie</button>
            
         <div id="login" className="loginForm" style={{display:"block"}}>
        <button onClick={closeLoginForm}  className="closeSection" >&times;</button>
        <form className="modal-content animate">
         <div style={{paddingBottom:"20px"}}>
         <img src={deus_ex_machina} alt="logo" />
         </div>
         <label style={{color:"black" , fontSize:"16px", float:"left"}} for="uname"><b>Title *</b></label>
         <input className="loginFormInput" onChange={checkIfInputsAreEmpty} type="text" id="title" name="uname" placeholder="Enter Title"/>
         <label style={{color:"black", fontSize:"16px", float:"left"}} for="pwd"><b>Publication Date</b></label>
         <input className="loginFormInput" onChange={checkIfInputsAreEmpty} type="text" id="pubdate" name="pwd" placeholder="Enter Publication Date"/>
         <label style={{color:"black", fontSize:"16px", float:"left"}} for="pwd"><b>Duration</b></label>
         <input className="loginFormInput" onChange={checkIfInputsAreEmpty} type="text" id="duration" name="pwd" placeholder="Enter Duration"/>
         <label style={{color:"black", fontSize:"16px", float:"left"}} for="pwd"><b>Rating</b></label>
         <input className="loginFormInput" onChange={checkIfInputsAreEmpty} type="text" id="rating" name="pwd" placeholder="Enter Rating"/>
         <label style={{color:"black", fontSize:"16px", float:"left"}} for="pwd"><b>Description</b></label>
         <input className="loginFormInput" onChange={checkIfInputsAreEmpty} type="text" id="description" name="pwd" placeholder="Enter Description"/>
         <label style={{color:"black", fontSize:"16px", float:"left"}} for="categories"><b>Categories</b></label>
         <input className="loginFormInput" onChange={checkIfInputsAreEmpty} type="text" id="categories" name="pwd" placeholder="Enter Description"/>
         <p style={{ color: "red", textAlign: "left",padding: "2em 0", fontSize: "16px" }}>Inputs with * are required</p>
         <p id="success" style={{ color: "green", textAlign: "center",padding: "2em 0", fontSize: "16px", display:"none" }}>You add succesfully a new movie!</p>
        </form>
      </div>
            <Menu admin={true} changePages={changePages} />
     </div>

        )
    }
}
}
export default AddAMovie