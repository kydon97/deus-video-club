import React from 'react';
import deus_ex_machina from '../images/deus_ex_machina.png';
import { login } from "../utils/api";
import '../styles/header.css';
import Profile from "./Profile";
import AdminProfile from "./AdminProfile";
import { BrowserRouter as Router, Route, Redirect, withRouter  } from "react-router-dom";

class Header extends React.Component {
  state = {
   isLoggedIn : false,
   isAdmin: false,
   username : "",
   token: ""
  }
  render() {

    if (this.state.isLoggedIn === true) {
      if(this.state.isAdmin === true) {

        return (
          <Router>
        <Redirect to="/profile"/>;
        <Route exact path="/profile" component={withRouter(() =>  <AdminProfile username={this.state.username} token={this.state.token}/>)}/>      
       </Router>
        )

      } else {

        return (
          <Router>
        <Redirect to="/profile"/>;
        <Route exact path="/profile" component={withRouter(() =>  <Profile username={this.state.username} token={this.state.token}/>)}/>      
       </Router>
        )
      }
    } else {
  //check if input fields of login form is empty or not so we disable login button or not.
  function checkIfInputsAreEmpty() {
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;
    const loginButton = document.getElementById("loginButton");
    if(username === "" || password === "") {
       loginButton.style.pointerEvents = "none";
       loginButton.style.cursor = "not-allowed";
       loginButton.style.opacity = ".8";
    } else {
      loginButton.style.pointerEvents = "auto";
      loginButton.style.cursor = "pointer";
      loginButton.style.opacity = "1re";
    }
  }

  //show login form modal
  function showLoginForm() {   
  const loginForm = document.getElementById("login");
  const loginButton = document.getElementById("loginBtn");
  loginForm.style.display="block";
  loginButton.style.display="none";
  }

  //cloe login form modal
  function closeLoginForm() { 
    const loginForm = document.getElementById("login");
    const loginButton = document.getElementById("loginBtn");
    loginForm.style.display="none";
    loginButton.style.display="block";
  }

  //make login request
  const loginRequest = () => {
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;
    var formData = new FormData();
    formData.append("username", username);
    formData.append("password", password);

    login(formData).then(res => {
      if (res.access === undefined) {
        document.getElementById("errorsection").style.display = "block";
      } else if (res.access !== undefined) {
        this.setState({
          isLoggedIn: true,
          username: username,
          token: res.access
        });
        if(this.state.username.includes("admin")) {
          this.setState({
            isAdmin:true
          })
        }
      }
      else {
        document.getElementById("errorsection").style.display = "block";
        document.getElementById("errorMessage").value = "Something Went Wrong...Please try again"
      }
    }).catch(error => {
      console.log("error: " + error);
    })
}
  return (
    <header className="App-header">
        <a href="https://ekmechanes.com/" target="_blank">
        <img src={deus_ex_machina} className="App-logo" alt="logo" />
        </a>
        <p>
          Welcome To Deus Video Club
        </p>
        <button id="loginBtn" className="loginButton" onClick={showLoginForm}>Login</button>

      <div id="login" className="loginForm">
        <button onClick={closeLoginForm}  className="closeSection" >&times;</button>
        <form className="modal-content animate">
         <div style={{paddingBottom:"20px"}}>
         <img src={deus_ex_machina} alt="logo" />
         </div>
         <label style={{color:"black" , fontSize:"16px", float:"left"}} for="uname"><b>Username</b></label>
         <input className="loginFormInput" onChange={checkIfInputsAreEmpty} type="text" id="username" name="uname" placeholder="Enter Username"/>
         <label style={{color:"black", fontSize:"16px", float:"left"}} for="pwd"><b>Password</b></label>
         <input className="loginFormInput" onChange={checkIfInputsAreEmpty} type="password" id="password" name="pwd" placeholder="Enter Password"/>

         <div className="sectionerror" id="errorsection">
         <p id="errorMessage" style={{color:"red" , fontSize:"15px"}}>
          No active account found with the given credentials
         </p>
        </div>
        </form>
        <button id="loginButton" className="loginButtonForm" onClick={loginRequest}>Login</button>
      </div>
      </header>
  );
}}}
export default Header;
