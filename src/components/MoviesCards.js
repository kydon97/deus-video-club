
import React from 'react';
import '../styles/dashboard.css';
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle
} from "reactstrap";

class MoviesCards extends React.Component {
  render() {
    function showDescription(description,uuid) {
      const descriptionSection = document.getElementById("description");
      const textOfDescription = document.getElementById("textOfDescription");
      const uuidOfMovie = document.getElementById("uuidOfMovie");
      descriptionSection.style.display="block";
      textOfDescription.style.whiteSpace = "normal";
      textOfDescription.style.paddingTop = "0px";
      textOfDescription.textContent = `${description}`;
      uuidOfMovie.textContent = `${uuid}`;
    return;
  }

      return (
        <div className="row">
        {this.props.cards.map((card) => (
          <Card
            body
            inverse
            style={{
              boxShadow: "2px 2px 23px white",
              width: "58%",
              color: "white",
              padding: "10px",
              margin: "10px",
              cursor: "pointer"
            }}
            onClick={() => showDescription(card.description,card.uuid)}
          >
            <CardImg
              top
              width="100%"
              src={card.poster_url}
              alt={card.title + " Image"} 
            />
            <CardBody>
              <CardTitle>Title: {card.title}</CardTitle>
              <CardText>Duration: {card.duration} minutes</CardText>
              <CardText>Date: {card.pub_date}</CardText>
              <CardText>Rating: {card.rating}</CardText>
              <CardText>Categories: {card.categories}</CardText>
              <button id="seeDescriptionButton" className="seeDescription" onClick={() => showDescription(card.description,card.uuid)}>
                Click To See the description
              </button>
            </CardBody>
          </Card>
        ))}
        ;
      </div>
      )
  }
}


export default MoviesCards;