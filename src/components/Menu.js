import React from 'react';
import '../App.css';

class Menu extends React.Component {
  render() {
    if(this.props.admin === true) {
      return (
        <div className="navMenu">
            <button
        className="App-link"
        onClick={() => this.props.changePages("movies")}
        >
        Rentals
        </button>
            <button
        className="App-link"
        onClick={() => this.props.changePages("addAMovie")}
        >
        Add a Movie
        </button>
        <button
        className="App-link"
        onClick={() => this.props.changePages("logout")}
        >
        Log out
        </button>
        </div>
          );
    } else {
      return (
        <div className="navMenu">
            <button
        className="App-link"
        onClick={() => this.props.changePages("movies")}
        >
        Movies
        </button>
            <button
        className="App-link"
        onClick={() => this.props.changePages("profile")}
        >
        Profile
        </button>
        <button
        className="App-link"
        onClick={() => this.props.changePages("rentals")}
        >
        My Rentals
        </button>
        <button
        className="App-link"
        onClick={() => this.props.changePages("logout")}
        >
        Log out
        </button>
        </div>
          );
    }
  }
}

export default Menu;
