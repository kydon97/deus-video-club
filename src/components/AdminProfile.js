import React from "react";
import "../styles/dashboard.css";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  withRouter,
} from "react-router-dom";
import Rentals from "./Rentals";
import Header from "./Header";
import Menu from "./Menu";
import { getProfile , getActiveRentals , getCountOfActiveRentals } from "../utils/api";
import ProfileCard from "./ProfileCard";
import AddAMovie from "./AddAMovie";

class AdminProfile extends React.Component {
  state = {
    count:null,
    isClickedMovies: false,
    isClickedAddAMovie: false,
    isClickedLogOut: false,
    firstName: "",
    lastName:"",
    email:"",
    wallet: "",
    activeRentals: [],
    titledesc:false,
    rentaldesc:false
  };

  componentDidMount() {
    getProfile(this.props.token).then(res =>{
        this.setState({
          firstName : res.first_name,
          lastName : res.last_name,
          email: res.email,
          wallet: res.wallet
        })
    }).catch(error => {
        console.log("Error: " + error);
    })
  
  getCountOfActiveRentals(this.props.token).then(res =>{
   this.setState({
     count: res.count
   })
  getActiveRentals(this.props.token,this.state.count).then(res =>{
    this.setState({
      activeRentals : res.results
    })
}).catch(error => {
    console.log("Error: " + error);
})
}).catch(error => {
    console.log("Error: " + error);
})

  }
  render() {
    if(this.state.count === null) {
        return (
           <p style={{ color: "white" }}>Please Wait...</p>
        )
    }else {
    if (this.state.isClickedMovies === true) {
      return (
        <Router>
          <Redirect to="/rentals" />;
          <Route
            exact
            path="/rentals"
            component={withRouter(() => (
              <Rentals
                username={this.props.username}
                token={this.props.token}
                admin={true}
              />
            ))}
          />
        </Router>
      );
    } else if (this.state.isClickedAddAMovie) {
      return (
        <Router>
          <Redirect to="/addAMovie" />;
          <Route
            exact
            path="/addAMovie"
            component={withRouter(() => (
              <AddAMovie
                username={this.props.username}
                token={this.props.token}
              />
            ))}
          />
        </Router>
      );
    } else if (this.state.isClickedLogOut) {
      return (
        <Router>
          <Redirect to="/" />;
          <Route
            exact
            path="/"
            component={withRouter(() => (
              <Header />
            ))}
          />
        </Router>
      );
    } else {
      const changePages = (type) => {
        if (type === "addAMovie") {
          this.setState({
            isClickedMovies: false,
            isClickedAddAMovie: true,
            isClickedLogOut: false,
          });
        } else if (type === "logout") {
          this.setState({
            isClickedMovies: false,
            isClickedAddAMovie: false,
            isClickedLogOut: true,
          });
        } else if (type === "movies") {
          this.setState({
            isClickedMovies: true,
            isClickedAddAMovie: false,
            isClickedLogOut: false,
          });
        }
      };
      return (

        <div style={{width:"100%"}}>
          <p style={{ color: "white", padding: "2em 0", fontSize: "24px" }}>
            Welcome {this.props.username}, this is your admin profile. 
          </p>
          <Menu admin={true} changePages={changePages} />
          <p style={{ color: "white", paddingTop:"1em", fontSize: "24px" }}>
            Personal Details:
          </p>
          <ProfileCard firstName={this.state.firstName} lastName={this.state.lastName} email={this.state.email} wallet={this.state.wallet}/>

        </div>
      
      );
    }
  }
}
}

export default AdminProfile;
