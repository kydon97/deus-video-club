import React from 'react';
import deus_ex_machina from '../images/deus_ex_machina.png';
import user from "../images/user.svg";
import '../styles/dashboard.css';
import {
    Card,
    CardImg,
    CardText,
    CardBody
  } from "reactstrap";

class ProfileCard extends React.Component {
    render() {
        return (
            <div className="rowProfile">
            <Card
            body
            inverse
            style={{
              boxShadow: "2px 2px 23px white",
              width: "58%",
              color: "white",
              padding: "10px",
              margin: "10px",
              cursor: "pointer"
            }}
            // onClick={() => showDescription(card.description,card.uuid)}
          >
              <div style={{borderBottom: "2px solid white", width:"100%", marginBottom: "40px"}}>
            <CardImg
              top
              width="25%"
              src={user}
              style={{padding: "20px 0 40px 0"}}
            />
            </div>
            <CardBody>
              <CardText style={{ backgroundColor: "white", color: "black",padding: "10px 0"}}>First Name: {this.props.firstName}</CardText>
              <CardText>Last Name: {this.props.lastName} </CardText>
              <CardText style={{ backgroundColor: "white", color: "black",padding: "10px 0"}} >Email: {this.props.email}</CardText>
              <CardText>Wallet: {this.props.wallet}</CardText>
            </CardBody>
          </Card>
      
            </div>
        )
    }
}

export default ProfileCard;