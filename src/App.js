import React from 'react';
import './App.css';
import Header from "./components/Header.js";
import Menu from "./components/Menu.js";
import SlideBars from "./components/SlideBars.js";
 
function App() {
  return (
    <div className="App">
      <Header/>
    </div>
  );
}

export default App;
