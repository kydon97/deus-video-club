import { API_URL } from "./constants";

export function login(formData) {
  const proxyurl = "https://cors-anywhere.herokuapp.com/";
    const url = proxyurl +API_URL+"auth/login/";
            // Display the values
for (var value of formData.values()) {
  }
    return fetch(url , {
      method: "POST",
      body: formData
    }).then(res => {
      const results = res.json();
      const { status, value} = res;
      try {
        if (status === 200 || status === 401) {
          return results;
        } else {
          throw Error(`Error Tracking request failed with status ${status}`);
        }
      } finally {
      }
    });
  }

  export function getAllMovies(token,numberOfMovies) {
    const proxyurl = "https://cors-anywhere.herokuapp.com/";
      const url = proxyurl +API_URL+`rent-store/movies/?page_size=${numberOfMovies}`;
      //console.log("Token : "+ token);
      return fetch(url , {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`
        }
      }).then(res => {
        const results = res.json();
        const { status } = res;
        //console.log("Status: " + status)
        try {
          if (status === 200 || status === 401) {
            return results;
          } else {
            throw Error(`Error Tracking request failed with status ${status}`);
          }
        } catch(error) {
          throw Error(`Error : ${error}`)
        }
      });
    }

    export function getCountOfMovies(token) {
      const proxyurl = "https://cors-anywhere.herokuapp.com/";
        const url = proxyurl +API_URL+"rent-store/movies/";
        //console.log("Token : "+ token);
        return fetch(url , {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`
          }
        }).then(res => {
          const results = res.json();
          const { status } = res;
          //console.log("Status: " + status)
          try {
            if (status === 200 || status === 401) {
              return results;
            } else {
              throw Error(`Error Tracking request failed with status ${status}`);
            }
          } catch(error) {
            throw Error(`Error : ${error}`)
          }
        });
      }

      export function rentMovie(formData,token) {
        const proxyurl = "https://cors-anywhere.herokuapp.com/";
          const url = proxyurl +API_URL+"rent-store/rentals/";
                  // Display the values
      for (var value of formData.values()) {
        //console.log(value); 
        }
          return fetch(url , {
            method: "POST",
            headers: {
              Authorization: `Bearer ${token}`
            },
            body: formData
          }).then(res => {
            const results = res.json();
            const { status } = res;
            try {
              if (status === 201 ) {
                return results;
              } else if (status === 400) {
                return status;
              } else {
                throw Error(`Error Tracking request failed with status ${status}`);
              }
            } finally {
            }
          });
        }

  export function getProfile(token) {
          const proxyurl = "https://cors-anywhere.herokuapp.com/";
          const url = proxyurl +API_URL+"rent-store/profile/";
            //console.log("Token : "+ token);
            return fetch(url , {
              method: "GET",
              headers: {
                Authorization: `Bearer ${token}`
              }
            }).then(res => {
              const results = res.json();
              const { status } = res;
              //console.log("Status: " + status)
              try {
                if (status === 200) {
                  return results;
                } else {
                  throw Error(`Error Tracking request failed with status ${status}`);
                }
              } catch(error) {
                throw Error(`Error : ${error}`)
              }
            });
          }

export function getCountOfActiveRentals(token) {
            const proxyurl = "https://cors-anywhere.herokuapp.com/";
            const url = proxyurl +API_URL+"rent-store/rentals/?only-active";
              //console.log("Token : "+ token);
              return fetch(url , {
                method: "GET",
                headers: {
                  Authorization: `Bearer ${token}`
                }
              }).then(res => {
                const results = res.json();
                const { status } = res;
                //console.log("Status: " + status)
                try {
                  if (status === 200) {
                    return results;
                  } else {
                    throw Error(`Error Tracking request failed with status ${status}`);
                  }
                } catch(error) {
                  throw Error(`Error : ${error}`)
                }
              });
            }

  export function getActiveRentals(token,numberOfActiveRentals) {
            const proxyurl = "https://cors-anywhere.herokuapp.com/";
            const url = proxyurl +API_URL+`rent-store/rentals/?only-active&page_size=${numberOfActiveRentals}`;
              //console.log("Token : "+ token);
              return fetch(url , {
                method: "GET",
                headers: {
                  Authorization: `Bearer ${token}`
                }
              }).then(res => {
                const results = res.json();
                const { status } = res;
                //console.log("Status: " + status)
                try {
                  if (status === 200) {
                    return results;
                  } else {
                    throw Error(`Error Tracking request failed with status ${status}`);
                  }
                } catch(error) {
                  throw Error(`Error : ${error}`)
                }
              });
            }


            export function returnMovie(token,uuid) {

            const proxyurl = "https://cors-anywhere.herokuapp.com/";
            const url = proxyurl +API_URL+`rent-store/rentals/${uuid}`;
            //console.log("Token : "+ token);
            //console.log("uuid : " + uuid);
            return fetch(url , {
              method: 'PATCH',
              headers: {
                Authorization: `Bearer ${token}`
              }
            }).then(res => {
              const results = res.json();
              const { status } = res;
              //console.log("Status: " + status)
              try {
                if (status === 200 || status === 400) {
                  return status;
                } else {
                  throw Error(`Error Tracking request failed with status ${status}`);
                }
              } catch(error) {
                throw Error(`Error : ${error}`)
              }
            });


            }


            export function getCountOfRentals(token) {
              const proxyurl = "https://cors-anywhere.herokuapp.com/";
              const url = proxyurl +API_URL+"rent-store/rentals/";
                //console.log("Token : "+ token);
                return fetch(url , {
                  method: "GET",
                  headers: {
                    Authorization: `Bearer ${token}`
                  }
                }).then(res => {
                  const results = res.json();
                  const { status } = res;
                  //console.log("Status: " + status)
                  try {
                    if (status === 200) {
                      return results;
                    } else {
                      throw Error(`Error Tracking request failed with status ${status}`);
                    }
                  } catch(error) {
                    throw Error(`Error : ${error}`)
                  }
                });
              }
  
    export function getRentals(token,numberOfActiveRentals) {
              const proxyurl = "https://cors-anywhere.herokuapp.com/";
              const url = proxyurl +API_URL+`rent-store/rentals/?page_size=${numberOfActiveRentals}`;
                //console.log("Token : "+ token);
                return fetch(url , {
                  method: "GET",
                  headers: {
                    Authorization: `Bearer ${token}`
                  }
                }).then(res => {
                  const results = res.json();
                  const { status } = res;
                  //console.log("Status: " + status)
                  try {
                    if (status === 200) {
                      return results;
                    } else {
                      throw Error(`Error Tracking request failed with status ${status}`);
                    }
                  } catch(error) {
                    throw Error(`Error : ${error}`)
                  }
                });
              }

       export function addNewMovie(token,formData) {

        const proxyurl = "https://cors-anywhere.herokuapp.com/";
        const url = proxyurl +API_URL+"rent-store/movies/";
                // Display the values
    for (var value of formData.values()) {
      //console.log(value); 
      }
        return fetch(url , {
          method: "POST",
          headers: {
            Authorization: `Bearer ${token}`
          },
          body: formData
        }).then(res => {
          const results = res.json();
          const { status } = res;
          try {
            if (status === 201 ) {
              return results;
            } else if (status === 400) {
              return results;
            } else {
              throw Error(`Error Tracking request failed with status ${status}`);
            }
          } finally {
          }
        });
       }

       export function getCategories(token) {

        const proxyurl = "https://cors-anywhere.herokuapp.com/";
              const url = proxyurl +API_URL+"rent-store/categories/";
                //console.log("Token : "+ token);
                return fetch(url , {
                  method: "GET",
                  headers: {
                    Authorization: `Bearer ${token}`
                  }
                }).then(res => {
                  const results = res.json();
                  const { status } = res;
                  //console.log("Status: " + status)
                  try {
                    if (status === 200) {
                      return results;
                    } else {
                      throw Error(`Error Tracking request failed with status ${status}`);
                    }
                  } catch(error) {
                    throw Error(`Error : ${error}`)
                  }
                })

       }



          